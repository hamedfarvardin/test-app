This application exposes a REST API with put/get/patch/delete functions,
the API is connected to a postgres table with below fields:

	'id': fields.Integer,
	'name': fields.String,
	'price': fields.Integer,
	'year': fields.String

1. to run with docker-compose:

docker-compose up --build -d


2. to run the app locally without wsgi:

a. spinup a postgres DB

    docker volume create --name db_volume
    docker run -d --name postgres -p 5432:5432 \
           --env-file docker/database.conf \
           -v db_volume:/var/lib/postgresql postgres:latest


b. export below env variables and run the app:

    export FLASK_APP=routes.py
    export POSTGRES_USER=postgres
    export POSTGRES_PASSWORD=postgres
    export POSTGRES_PORT=5432
    export POSTGRES_DB=example
    export POSTGRES_HOST=localhost
    flask run


* after each code change run below command to create the new tar file :

    python3 setup.py sdist


*    to test the api run test.py or :

    curl -XPUT -H "Content-type: application/json" -d \
    '{"name": "test1", "price": 5000, "year": "1987"}' \
    '127.0.0.1:500/myapi/0'


    curl -XGET -H "Content-type: application/json"  \
    '127.0.0.1:500/myapi/0'


    /******/

TT repo :  

local gitlab

sudo docker run --detach  --hostname gitlab.example.com  --publish 443:443 --publish 80:80 --publish 22:22  --name gitlab  --restart always  gitlab/gitlab-ce:latest

gitlab access token : AWK3v8755C5R2NmyC6DQ

## create ssh key on wsl ubunto
sudo apt install openssh-client
ssh-keygen -t rsa

