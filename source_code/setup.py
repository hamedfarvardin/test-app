from setuptools import find_packages, setup

setup(
    name="example-app",
    version="0.1.0",
    author="Majid",
    author_email="majid.eteghad@tecnotree.com",
    description="An example app",
    keywords="Flask example",
    include_package_data = True,
    url=("https://tecnotree.com"),
    python_requires='~=3.6',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    zip_safe=False,
    install_requires=[
        "psycopg2==2.7.6.1",
        "uwsgi==2.0.17.1",
        "aniso8601==8.0.0",
        "click==7.1.2",
        "Flask==1.1.2",
        "Flask-RESTful==0.3.8",
        "Flask-SQLAlchemy==2.4.3",
        "itsdangerous==1.1.0",
        "Jinja2==2.11.2",
        "MarkupSafe==1.1.1",
        "pytz==2020.1",
        "six==1.15.0",
        "SQLAlchemy==1.3.18",
        "Werkzeug==1.0.1",
        "requests==2.25.1"
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
