import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()


class Table(db.Model):
    __tablename__ = 'car-table'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    price = db.Column(db.Integer)
    year = db.Column(db.String(100))
