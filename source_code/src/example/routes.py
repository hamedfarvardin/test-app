import json
from requests import NullHandler

from flask import request,render_template
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with

from . import create_app
from .models import Table, db

app = create_app()
thisapi = Api(app)


resource_fields = {
	'id': fields.Integer,
	'name': fields.String,
	'price': fields.Integer,
	'year': fields.String
}

put_args = reqparse.RequestParser()
put_args.add_argument("name", type=str, help="Name is required", required=True)
put_args.add_argument("price", type=int, help="price is required", required=True)
put_args.add_argument("year", type=str, help="year is required", required=True)

update_args = reqparse.RequestParser()
update_args.add_argument("name", type=str, help="Name is required")
update_args.add_argument("price", type=int, help="price is required")
update_args.add_argument("year", type=str, help="year is required")

# Route the user to the homepage
@app.route('/', methods = ['GET'])
def home():
    return render_template('index.html')

@app.route('/about', methods = ['GET'])
def about():
    return render_template('about.html')

@app.route('/contact', methods = ['GET'])
def contact():
    return render_template('contact.html')

@app.route('/portfolio', methods = ['GET'])
def portfolio():
    return render_template('portfolio.html')	

class myapi(Resource):
 #serilizing the data output based on defined resource_fields           	
	@marshal_with(resource_fields)
	def get(self, entered_id):
		result = Table.query.filter_by(id=entered_id).first()
		if not result:
			abort(404, message="Could not find data with that id")
		return result

	@marshal_with(resource_fields)
	def put(self, entered_id):
#adding parsed and validated  values to arg variable                   		
		args = put_args.parse_args()
#adding posted details to videos list with vide_id key            
		result = Table.query.filter_by(id=entered_id).first()
		if result:
			abort(409, message="id taken...")

		data = Table(id=entered_id, name=args['name'], price=args['price'], year=args['year'])
		db.session.add(data)
		db.session.commit()
#changing default 200 response to 201            
		return data, 201

	@marshal_with(resource_fields)
	def patch(self, entered_id):
		args = update_args.parse_args()
		result = Table.query.filter_by(id=entered_id).first()
		if not result:
			abort(404, message="ID doesn't exist, cannot update")

		if args['name']:
			result.name = args['name']
		if args['price']:
			result.price = args['price']
		if args['year']:
			result.year = args['year']

		db.session.commit()

		return result


	def delete(self, entered_id):
		args = update_args.parse_args()
		result = Table.query.filter_by(id=entered_id).first()
		if not result:
			abort(404, message="ID doesn't exist")    		
		db.session.delete(result)
		db.session.commit()
		return '', 204



thisapi.add_resource(myapi, "/myapi/<int:entered_id>")


