import requests

BASE = "http://127.0.0.1:500/"

data = [{"year": 10,"name": 'majid' ,"price": 10000},
        {"year": 20,"name": 'ali' ,"price": 20000},
        {"year": 30,"name": 'jim' ,"price": 30000},
        {"year": 40,"name": 'jane' ,"price": 40000}]


for i in range(len(data)):
    response = requests.put(BASE + "myapi/" + str(i), data[i])
    print(response.json())
            
input()
response = requests.get(BASE + "myapi/2")
print(response.json())
input()
response = requests.delete(BASE + "myapi/0")
print(response)
input()
response = requests.patch(BASE + "myapi/2",{"price":100})
print(response.json())
input()
response = requests.get(BASE + "myapi/2")
print(response.json())
input()
response = requests.get(BASE + "myapi/0")
print(response.json())


